# AMP GAN
### Facilitating the design of Anti-Microbial Peptides using Generative Adversarial Networks

## Getting Started
AMPGAN is built using Python and Tensorflow.
We recommend the use of [Anaconda](https://www.anaconda.com) to manage your Python environment.

Additionally, we use [Git Large File Storage](https://git-lfs.github.com/) (LFS) to manage data and model checkpoints.
Ensure that you have it installed before cloning this repository.

Clone the repository:
```bash
# Via SSH
git clone git@gitlab.com:vail-uvm/amp-gan.git

# Via HTTPS
git clone https://gitlab.com/vail-uvm/amp-gan.git
```

With Anaconda and the provided environment file, setup can be completed in a single line:
```bash
conda env create -f ampgan.yaml
```
This will create a new Anaconda environment with all the dependencies required to run any code in this repository.

## Contents
| Name           | Description |
| ---            | --- |
| ampgan/        | Primary source code folder. |
| data/          | Contains all raw data used to construct AMPGAN training sets. |
| models/        | Contains the weights of a trained AMPGANv2 model. |
| results/       | All code outputs will be saved here. |
| scripts/       | Utility scripts, mostly for data or experiment management. |
| .gitattributes | Controls Git LFS behavior. |
| .gitignore     | Determines which files are considered by Git. |
| ampgan.yaml    | Anaconda environment specification. |
| LICENSE        | MIT license. |
| README.md      | This file. |

## Related Publications:
 - [A Generative Approach Toward Precision Antimicrobial Peptide Design](https://www.biorxiv.org/content/10.1101/2020.10.02.324087v2)
 - [AMPGAN v2: Machine Learning Guided Discovery of Anti-Microbial Peptides](https://www.biorxiv.org/content/10.1101/2020.11.18.388843v2)
