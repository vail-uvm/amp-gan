# Rename all .txt files in the current directory to .csv
for f in *.txt; do 
    mv -- "$f" "${f%.txt}.csv"
done

