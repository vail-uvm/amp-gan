#!/usr/bin/env bash
# Scrapes AMP data from the DBAASP database, https://dbaasp.org/, using the
# HTTPS API. Data is obtained and consolidated in the JSON format.


mkdir -p ../data/dbaasp


echo "[
" > ../data/dbaasp/raw.json


for i in {1..12376}
do
    curl -Ls -d "query=peptide_card&peptide_id=$i&format=json" https://dbaasp.org/api/v1 >> ../data/dbaasp/raw.json
    echo ",
    " >> ../data/dbaasp/raw.json
done


echo "]" >> ../data/dbaasp/raw.json
