#!/usr/bin/env bash
# Creates a MP4 video to qualitatively summarize the training progress of a GAN trained on MNIST.

DIR="${1:-../results/mnist_gen_gan}"

ffmpeg -r 10 -f image2 -i "$DIR/%d.png" -vcodec libx264 -crf 25 -pix_fmt yuv420p "$DIR/gen_video.mp4"
