#!/usr/bin/env bash
# Runs 30 trials of AMPGAN via slurm. The results can be used to assess training stability.

for i in {1..30} ; do
    sbatch submit.sh $i
done
