"""
Loads a trained GAN and probes the sensitivity of the discriminator to perturbations
of the conditioning vectors.
"""

import argparse

import data_utils
import model  # Needed for correct loading of custom model
import numpy as np
import pandas as pd
import tensorflow as tf


def get_parser():
    parser = argparse.ArgumentParser(
        description="Investigates the sensitivity of the Discriminator to conditioning vector perturbations.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument("model_checkpoint", help="Path to a trained GAN checkpoint.")

    return parser


def main(model_checkpoint: str, batch_size: int = 256):
    # Load the AMP data
    data = data_utils.get_train_data(batch_size)
    data.on_epoch_end()

    # Load the GAN
    gan = tf.keras.models.load_model(model_checkpoint)

    (
        len_target_groups,
        len_targets,
        len_mic50,
        len_seq_len,
    ) = data_utils.get_condition_option_counts()
    target_groups_slice = slice(1, 1 + len_target_groups)
    targets_slice = slice(1 + len_target_groups, 1 + len_target_groups + len_targets)
    mic50_slice = slice(
        1 + len_target_groups + len_targets,
        1 + len_target_groups + len_targets + len_mic50,
    )
    seq_len_slice = slice(
        1 + len_target_groups + len_targets + len_mic50,
        1 + len_target_groups + len_targets + len_mic50 + len_seq_len,
    )

    total_validity = []
    original_validity = []
    target_groups_p_validity = []
    targets_p_validity = []
    mic50_validity = []
    length_validity = []
    for i in range(len(data)):
        sequences, conditions = data[i]
        condition_len = conditions.shape[1]

        for j in range(batch_size // 2):
            sequences_ = np.tile(sequences[j], (condition_len + 1, 1, 1))
            conditions_ = np.tile(conditions[j], (condition_len + 1, 1))
            conditions_[1:] = np.logical_xor(conditions_[1:], np.eye(condition_len))

            validity = gan.discriminator([sequences_, conditions_]).numpy().flatten()

            original_validity.append(validity[0])

            # Make everything relative to the validity of the original sample
            validity -= validity[0]

            total_validity.extend(validity)
            target_groups_p_validity.extend(validity[target_groups_slice])
            targets_p_validity.extend(validity[targets_slice])
            mic50_validity.extend(validity[mic50_slice])
            length_validity.extend(validity[seq_len_slice])

    print(pd.Series(total_validity, name="total_validity").describe())
    print(pd.Series(original_validity, name="original_validity").describe())
    print(
        pd.Series(target_groups_p_validity, name="target_groups_p_validity").describe()
    )
    print(pd.Series(targets_p_validity, name="targets_p_validity").describe())
    print(pd.Series(mic50_validity, name="mic50_validity").describe())
    print(pd.Series(length_validity, name="length_validity").describe())


if __name__ == "__main__":
    main(**vars(get_parser().parse_args()))
