from pathlib import Path


def valid_file(file_path: str) -> Path:
    file_path = Path(file_path)
    if file_path.exists():
        return file_path
    else:
        raise FileNotFoundError(f"{str(file_path)} does not exist.")


def file_make_parents(file_path: str) -> Path:
    p = Path(file_path)
    p.parent.mkdir(exist_ok=True, parents=True)
    return p
