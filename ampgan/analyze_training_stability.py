import argparse
import logging
import multiprocessing
from pathlib import Path
from typing import List

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib.cm import get_cmap
from matplotlib.colors import rgb2hex

logger = logging.getLogger(__name__)


def get_parser():
    parser = argparse.ArgumentParser(
        description="Analyzes the training stability of AMPGAN using the results of several independent trials.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    parser.add_argument(
        "result_dir",
        type=Path,
        help="Folder containing the results of several training runs.",
    )
    return parser


def main(result_dir: Path):
    assert result_dir.is_dir()

    logging.basicConfig(
        filename=str(result_dir / "training_stability.log"),
        filemode="w",
        level=logging.INFO,
        format="%(message)s",
    )

    dfs = [pd.read_csv(f) for f in result_dir.glob("losses*.csv")]
    agg_df = pd.concat(dfs)
    good_dfs = [x for x in dfs if training_success(x)]
    try:
        good_df = pd.concat(good_dfs)
    except ValueError:
        good_df = pd.DataFrame(columns=agg_df.columns)

    bad_dfs = [x for x in dfs if not training_success(x)]
    try:
        bad_df = pd.concat(bad_dfs)
    except ValueError:
        bad_df = pd.DataFrame(columns=agg_df.columns)

    if "reg_strength" in agg_df.columns:
        agg_df.drop("reg_strength", axis=1, inplace=True)
        good_df.drop("reg_strength", axis=1, inplace=True)
        bad_df.drop("reg_strength", axis=1, inplace=True)

    logger.info(f"Training successes: {len(good_dfs)} / {len(dfs)}")

    with multiprocessing.Pool() as pool:
        pool.starmap(
            make_line_plot,
            (
                (agg_df, result_dir / "aggregated_training_stability.png"),
                (good_df, result_dir / "good_training_stability.png"),
                (bad_df, result_dir / "bad_training_stability.png"),
            ),
        )
    make_line_plot(agg_df, result_dir / "aggregated_training_stability.png")
    make_line_plot(good_df, result_dir / "good_training_stability.png")
    make_line_plot(bad_df, result_dir / "bad_training_stability.png")


def training_success(df: pd.DataFrame) -> bool:
    """
    Evaluates a trial based on a heuristic success criteria.

    Goals:
        1) AMPGAN should be responsive to the sequence length provided in the conditioning vector
        2) The generated sequences should have a realistic amino acid diversity.

    Implementation:
        1) R^2 score between the desired and produced sequence length
        2) Shannon's entropy of a batch of sequences (Real sequences are somewhere between 2.5 and 3.5 on average)
    """
    good_r2 = df["g_r2_score"].values > 0.5
    good_entropy = (2 < df["g_seq_entropy"].values) & (df["g_seq_entropy"].values < 4)
    return np.any(good_r2 & good_entropy)


def make_line_plot(df: pd.DataFrame, out_file: Path):
    """
    Summarizes a training run using a stack of sub-figures that display various metrics using a shared x-axis.
    """
    sns.set_style("whitegrid")
    fig, axes = plt.subplots(len(df.columns) - 1, 1, sharex=True)
    handles, labels = [], []
    for ax, col, color in zip(
        axes, df.columns[1:], get_hex_colors(int(1.25 * len(df.columns)))
    ):
        sns.lineplot(
            x="epoch", y=col, data=df, ax=ax, color=color, label=col, legend=False,
        )
        ax.set_xlabel("")
        ax.set_ylabel("")
        handle, label = ax.get_legend_handles_labels()
        handles.append(handle[0])
        labels.append(label[0])

    def fix_label(l: str) -> str:
        return l.replace("_", " ").title()

    fig.legend(
        handles, [fix_label(l) for l in labels], loc="center right",
    )
    plt.xlabel("Epoch")
    fig.subplots_adjust(right=0.75)
    plt.savefig(out_file)
    plt.close()


def get_hex_colors(count, cmap_name="viridis") -> List[str]:
    cmap = get_cmap(cmap_name, count)
    return [rgb2hex(cmap(i)[:3]) for i in range(count)]


if __name__ == "__main__":
    main(**vars(get_parser().parse_args()))
