"""
Methods to load and clean the UniProt data for use in training machine learning models.
More information about the UniProt database can be found at https://www.uniprot.org/
"""
import json
import logging
from pathlib import Path
from typing import Optional

import data_utils
import dbaasp
import numpy as np
import pandas as pd

logger = logging.getLogger(__name__)


def prepare():
    test_path = Path("../data/dbaasp/clean.csv")
    if not test_path.exists():
        dbaasp.prepare()

    df = load_data(max_seq_len=32)

    logger.info(df.dtypes)

    logger.info("Calculating sequence symbol frequencies.")
    data_utils.calculate_symbol_frequencies(
        df["sequence"], dump_path="../data/uniprot/symbol_frequencies.json",
    )


def load_data(
    deduplicate: bool = True,
    drop_false_negatives: bool = True,
    kind: str = "all",
    load_path: str = "../data/uniprot/uniprot.tab",
    max_seq_len: int = 32,
    amp_sequences: Optional[pd.Series] = None,
):
    df = pd.read_csv(load_path, sep="\t")

    if deduplicate:
        df = df.drop_duplicates("Sequence")

    if max_seq_len is not None:
        df = df[df.Sequence.str.len() <= max_seq_len]

    if drop_false_negatives:
        if amp_sequences is None:
            amp_sequences = dbaasp.load_data(max_seq_len=max_seq_len).sequence
        fns = df.Sequence.isin(amp_sequences)
        df = df.loc[~fns.values]
        logger.info(f"{fns.sum()} AMP sequences removed from UniProt data.")

    df.columns = [data_utils.camel_to_snake_case(x) for x in df.columns]
    df["targets"] = [[] for _ in range(len(df))]
    df["target_groups"] = [[] for _ in range(len(df))]

    kind = kind.lower()
    if kind == "reviewed":
        out_df = df[df.Status == "reviewed"]
    elif kind == "unreviewed":
        out_df = df[df.Status == "unreviewed"]
    elif kind == "all":
        out_df = df
    else:
        raise ValueError(
            f'Received invalid value kind={kind}, expected one of {{"all", "reviewed", "unreviewed"}}.'
        )

    logger.info(f"{len(out_df.index)} samples after curating UniProt.")
    return out_df


def make_condition_vectors(df: pd.DataFrame, max_seq_len: int = 32) -> np.ndarray:
    with open("../results/target_groups_values.json") as f:
        target_groups_values = json.load(f)
    with open("../results/targets_values.json") as f:
        targets_values = json.load(f)
    with open("../results/mic50_bins.json") as f:
        mic50_bins = json.load(f)

    target_groups_value = np.zeros((len(df), len(target_groups_values)))

    targets_value = np.zeros((len(df), len(targets_values)))

    mic50 = len(mic50_bins) - 2 + np.zeros(len(df), dtype=int)
    mic50 = data_utils.to_categorical(mic50, num_classes=len(mic50_bins) - 1)

    lengths = data_utils.int_col_2_bin_mask(df.length.values, max_len=max_seq_len)

    conditions = np.concatenate(
        [target_groups_value, targets_value, mic50, lengths], axis=-1,
    )

    logger.info(f"Uniprot Target Groups Shape:    {target_groups_value.shape}")
    logger.info(f"Uniprot Targets Shape:          {targets_value.shape}")
    logger.info(f"Uniprot MIC 50 Shape:           {mic50.shape}")
    logger.info(f"Uniprot Sequence Lengths Shape: {lengths.shape}")
    logger.info(f"Uniprot Condition Shape:        {conditions.shape}")
    return conditions


if __name__ == "__main__":
    prepare()
