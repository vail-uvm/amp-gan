"""
Implements a Conditional Bidirectional WGAN-GP in order to learn how to generate
Anti-Microbial Peptides.

Implementation based on:
    https://keras.io/examples/generative/dcgan_overriding_train_step/
    https://keras.io/examples/generative/wgan_gp/
"""

import logging
from typing import Optional, Tuple

import numpy as np
import tensorflow as tf
import vailtools
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Model

logger = logging.getLogger(__name__)


def wgan_d_loss(real_logits, fake_logits):
    real_loss = tf.reduce_mean(real_logits)
    fake_loss = tf.reduce_mean(fake_logits)
    return fake_loss - real_loss


def wgan_g_loss(fake_logits):
    return -tf.reduce_mean(fake_logits)


class CWGANGP(keras.Model):
    """
    As noted in https://arxiv.org/abs/1704.00028, batch normalization in the critic
    leads to extremely poor performance. Ensure that it is not used.
    """

    def __init__(
        self,
        discriminator: keras.Model,
        generator: keras.Model,
        encoder: keras.Model,
        latent_dim: int = 256,
        discriminator_extra_steps: int = 5,
        gp_weight: float = 10.0,
    ):
        super().__init__()
        self.discriminator = discriminator
        self.generator = generator
        self.encoder = encoder
        self.latent_dim = latent_dim
        self.d_steps = discriminator_extra_steps
        self.gp_weight = gp_weight

        self.d_optimizer = None
        self.g_optimizer = None
        self.e_optimizer = None
        self.d_loss_fn = None
        self.g_loss_fn = None
        self.e_loss_fn = None

        # Dummy call to _set_inputs, needed to enable model saving.
        self._set_inputs(tf.random.normal(shape=(1,)))

        if any(
            isinstance(l, keras.layers.BatchNormalization)
            for l in self.discriminator.layers
        ):
            logger.warning(
                "Discriminator appears to contain batch normalization layers, which may lead to poor performance."
            )

    def compile(
        self,
        d_optimizer=keras.optimizers.Adam(learning_rate=0.0001, beta_1=0.0, beta_2=0.9),
        g_optimizer=keras.optimizers.Adam(learning_rate=0.0001, beta_1=0.0, beta_2=0.9),
        e_optimizer=keras.optimizers.Adam(learning_rate=0.0001, beta_1=0.0, beta_2=0.9),
        d_loss_fn=wgan_d_loss,
        g_loss_fn=wgan_g_loss,
        e_loss_fn=keras.losses.mse,
        **kwargs,
    ):
        super().compile(**kwargs)
        self.d_optimizer = d_optimizer
        self.g_optimizer = g_optimizer
        self.e_optimizer = e_optimizer
        self.d_loss_fn = d_loss_fn
        self.g_loss_fn = g_loss_fn
        self.e_loss_fn = e_loss_fn

    def gradient_penalty(self, batch_size: int, real_samples, fake_samples, conditions):
        """
        Outline:
            1. Get the discriminator output for a interpolated image.
            2. Calculate the gradients w.r.t to the interpolated image.
            3. Calculate the norm of the gradients
        """
        alpha = tf.random.normal([batch_size, 1, 1], 0.0, 1.0)
        diff = fake_samples - real_samples
        interpolated = real_samples + alpha * diff

        with tf.GradientTape() as gp_tape:
            gp_tape.watch(interpolated)
            gp_tape.watch(conditions)
            pred = self.discriminator([interpolated, conditions], training=True)

        grads = gp_tape.gradient(pred, [interpolated, conditions])[0]
        norm = tf.sqrt(tf.reduce_sum(tf.square(grads), axis=[1, 2]))
        return tf.reduce_mean((norm - 1.0) ** 2)

    def train_step(self, batch):
        """
        Outline:
            1. Train the discriminator and get the discriminator loss
            2. Calculate the gradient penalty
            3. Multiply this gradient penalty with a constant weight factor
            4. Add gradient penalty to the discriminator loss
            5. Train the generator and get the generator loss
            6. Return the losses as a dictionary.
        """
        real_samples, conditions = batch
        real_samples, conditions = (
            tf.cast(real_samples, tf.float32),
            tf.cast(conditions, tf.float32),
        )
        batch_size = tf.shape(real_samples)[0]

        # Discriminator updates
        for i in range(self.d_steps):
            latent_vectors = tf.random.normal(shape=(batch_size, self.latent_dim))
            with tf.GradientTape() as tape:
                fake_samples = self.generator(
                    [latent_vectors, conditions], training=True
                )
                fake_logits = self.discriminator(
                    [fake_samples, conditions], training=True
                )
                real_logits = self.discriminator(
                    [real_samples, conditions], training=True
                )

                d_loss = self.d_loss_fn(real_logits, fake_logits)
                gp = self.gradient_penalty(
                    batch_size, real_samples, fake_samples, conditions
                )
                d_loss += self.gp_weight * gp
            d_gradient = tape.gradient(d_loss, self.discriminator.trainable_variables)
            self.d_optimizer.apply_gradients(
                zip(d_gradient, self.discriminator.trainable_variables)
            )

        # Generator update
        latent_vectors = tf.random.normal(shape=(batch_size, self.latent_dim))
        with tf.GradientTape() as tape:
            fake_samples = self.generator([latent_vectors, conditions], training=True)
            logits = self.discriminator([fake_samples, conditions], training=True)
            g_loss = self.g_loss_fn(logits)
        gen_gradient = tape.gradient(g_loss, self.generator.trainable_variables)
        self.g_optimizer.apply_gradients(
            zip(gen_gradient, self.generator.trainable_variables)
        )

        log = {"d_loss": d_loss, "g_loss": g_loss, "gradient_penalty": gp}

        # Encoder update
        with tf.GradientTape() as tape:
            fake_samples = self.generator([latent_vectors, conditions], training=True)
            embedding = self.encoder([fake_samples, conditions], training=True)
            e_loss = self.e_loss_fn(latent_vectors, embedding)
        enc_gradient = tape.gradient(e_loss, self.encoder.trainable_variables)
        self.e_optimizer.apply_gradients(
            zip(enc_gradient, self.encoder.trainable_variables)
        )
        log["e_loss"] = e_loss

        return log

    def call(self, inputs, training=None, mask=None):
        logger.warning(
            "The call method is ambiguous for GAN models. Use generate or discriminate."
        )
        return inputs

    def generate(self, inputs, training=None, mask=None):
        return self.generator(inputs, training, mask)

    def discriminate(self, inputs, training=None, mask=None):
        return self.discriminator(inputs, training, mask)

    def compute_output_shape(self, input_shape):
        return input_shape


class CGAN(keras.Model):
    """
    Discriminator regularization based on:
        https://arxiv.org/abs/1705.09367
        https://arxiv.org/abs/1801.04406

    Default regularization parameter setting borrowed from:
        https://github.com/LMescheder/GAN_stability/blob/master/configs/default.yaml
    """

    def __init__(
        self,
        discriminator: keras.Model,
        generator: keras.Model,
        encoder: keras.Model,
        latent_dim: int = 256,
        reg_strength: float = 10.0,
    ):
        super().__init__()
        self.discriminator = discriminator
        self.generator = generator
        self.encoder = encoder
        self.latent_dim = latent_dim
        self.reg_strength = reg_strength
        self.d_optimizer = None
        self.g_optimizer = None
        self.e_optimizer = None
        self.loss_fn = None
        self.e_loss_fn = None

        # Dummy call to _set_inputs, needed to enable model saving.
        self._set_inputs(tf.random.normal(shape=(1,)))

    def compile(
        self,
        d_optimizer=tf.keras.optimizers.Adam(lr=0.0003),
        g_optimizer=tf.keras.optimizers.Adam(lr=0.0003),
        e_optimizer=tf.keras.optimizers.Adam(lr=0.0003),
        loss_fn=tf.keras.losses.BinaryCrossentropy(),
        e_loss_fn=keras.losses.mse,
        **kwargs,
    ):
        """
        Note:
            A linear discriminator output and BinaryCrossentropy(from_logits=True)
            seems to provide a worse training signal than a sigmoid discriminator
            and BinaryCrossentropy().
        """
        super().compile(**kwargs)
        self.d_optimizer = d_optimizer
        self.g_optimizer = g_optimizer
        self.e_optimizer = e_optimizer
        self.loss_fn = loss_fn
        self.e_loss_fn = e_loss_fn

    def gradient_penalty(self, real_samples, fake_samples, conditions):
        with tf.GradientTape() as tape:
            tape.watch(real_samples)
            tape.watch(conditions)
            pred = self.discriminator([real_samples, conditions], training=True)

        grads = tape.gradient(pred, [real_samples, conditions])[0]
        norm = tf.reduce_sum(tf.square(grads), axis=[1, 2])
        return tf.reduce_mean(norm)

    def train_step(self, batch):
        samples, conditions = batch
        samples, conditions = (
            tf.cast(samples, tf.float32),
            tf.cast(conditions, tf.float32),
        )
        batch_size = tf.shape(samples)[0]
        latent_vectors = tf.random.normal(shape=(batch_size, self.latent_dim))
        generated_samples = self.generator([latent_vectors, conditions])

        combined_samples = tf.concat([generated_samples, samples], axis=0)
        combined_conditions = tf.concat([conditions, conditions], axis=0)
        labels = tf.concat(
            [tf.ones((batch_size, 1)), tf.zeros((batch_size, 1))], axis=0,
        )

        # Train the discriminator
        with tf.GradientTape() as tape:
            predictions = self.discriminator([combined_samples, combined_conditions])
            d_loss = self.loss_fn(labels, predictions)
            if self.reg_strength > 0:
                gp = self.gradient_penalty(samples, generated_samples, conditions)
                d_loss += 0.5 * self.reg_strength * gp
        grads = tape.gradient(d_loss, self.discriminator.trainable_weights)
        self.d_optimizer.apply_gradients(
            zip(grads, self.discriminator.trainable_weights)
        )

        # Train the generator
        latent_vectors = tf.random.normal(shape=(batch_size, self.latent_dim))
        misleading_labels = tf.zeros((batch_size, 1))
        with tf.GradientTape() as tape:
            predictions = self.discriminator(
                [self.generator([latent_vectors, conditions]), conditions]
            )
            g_loss = self.loss_fn(misleading_labels, predictions)
        grads = tape.gradient(g_loss, self.generator.trainable_weights)
        self.g_optimizer.apply_gradients(zip(grads, self.generator.trainable_weights))

        # Train the encoder
        with tf.GradientTape() as tape:
            fake_samples = self.generator([latent_vectors, conditions], training=True)
            embedding = self.encoder([fake_samples, conditions], training=True)
            e_loss = self.e_loss_fn(latent_vectors, embedding)
        grads = tape.gradient(e_loss, self.encoder.trainable_variables)
        self.e_optimizer.apply_gradients(zip(grads, self.encoder.trainable_variables))

        return {"d_loss": d_loss, "g_loss": g_loss, "e_loss": tf.reduce_mean(e_loss)}

    def call(self, inputs, training=None, mask=None):
        logger.warning(
            "The call method is ambiguous for GAN models. Use generate or discriminate."
        )
        return inputs

    def compute_output_shape(self, input_shape):
        return input_shape

    def generate(self, inputs, training=None, mask=None):
        return self.generator(inputs, training, mask)

    def discriminate(self, inputs, training=None, mask=None):
        return self.discriminator(inputs, training, mask)

    def encode(self, inputs, training=None, mask=None):
        return self.encoder(inputs, training, mask)

    def get_config(self):
        base_config = super().get_config()
        config = {
            "latent_dim": self.latent_dim,
            "reg_strength": self.reg_strength,
        }
        return {**base_config, **config}


def amp_generator(
    output_shape: Tuple[int] = (32, 23),
    latent_shape: Tuple[int] = (256,),
    condition_shape: Tuple[int] = (64,),
    blocks: int = 6,
    filters: int = 128,
    final_activation: str = "tanh",
):
    latent_vectors = layers.Input(latent_shape, name="latent_vectors")
    conditions = layers.Input(condition_shape, name="conditions")
    features = layers.concatenate([latent_vectors, conditions])

    features = layers.Dense(np.product(output_shape))(features)
    features = layers.Activation(layers.LeakyReLU(alpha=0.2))(features)

    features = layers.Reshape(output_shape)(features)
    features = vailtools.layers.CoordinateChannel1D()(features)

    total_features = []
    for i in range(blocks):
        features = layers.Conv1D(
            filters=filters, kernel_size=3, dilation_rate=2 ** i, padding="same"
        )(features)
        features = layers.LeakyReLU(alpha=0.2)(features)
        total_features.append(features)

    features = layers.Concatenate()(total_features)
    features = layers.Conv1D(filters=output_shape[1], kernel_size=1, padding="same")(
        features
    )
    samples = layers.Activation(final_activation)(features)

    g = Model([latent_vectors, conditions], samples, name="generator")
    g.summary()
    return g


def amp_discriminator(
    sample_shape: Tuple[int] = (32, 23),
    condition_shape: Tuple[int] = (64,),
    output_dim: int = 1,
    feature_width: int = 256,
    filters: int = 64,
    blocks: int = 6,
    final_activation: Optional[str] = None,
    name: str = "discriminator",
):
    samples = layers.Input(sample_shape, name="samples")
    conditions = layers.Input(condition_shape, name="conditions")

    features = layers.Concatenate()(
        [samples, layers.RepeatVector(sample_shape[0])(conditions)]
    )
    features = vailtools.layers.CoordinateChannel1D()(features)

    for i in range(blocks):
        features = layers.SpatialDropout1D(rate=0.25)(features)
        features = layers.Conv1D(
            filters=filters, kernel_size=4, strides=2, padding="same"
        )(features)
        features = layers.LeakyReLU(alpha=0.2)(features)
    features = layers.GlobalAveragePooling1D()(features)

    for _ in range(3):
        features = layers.Dropout(rate=0.25)(features)
        features = layers.Dense(feature_width)(features)
        features = layers.LeakyReLU(alpha=0.2)(features)

    validity = layers.Dense(output_dim, activation=final_activation)(features)

    d = Model([samples, conditions], validity, name=name)
    d.summary()
    return d


def mnist_generator(
    output_shape: Tuple[int] = (1024, 1),
    latent_shape: Tuple[int] = (256,),
    condition_shape: Tuple[int] = (10,),
    blocks: int = 6,
    filters: int = 64,
    final_activation: str = "tanh",
    name: str = "generator",
):
    """
    Constructs and returns a conditional generator to be used as part of a GAN

    Uses Dense layers to mix the latent vector and conditioning vector,
    then uses exponentially dilated WaveNet blocks to reach a large effective
    receptive field.

    Args:
        output_shape: Tuple[int], Shape of a single generated sample.
        latent_shape: Tuple[int], Shape of a single latent vector.
        condition_shape: Tuple[int], Shape of a single condition vector.
        blocks: int, Number of convolutions to stack at the tail of the generator.
        filters: int, Number of filters used in convolution layers.
        final_activation: Activation function applied to network outputs.
        name: str, Model identifier.

    Returns: keras.models.Model
    """
    latent_vectors = layers.Input(latent_shape, name="latent_vectors")
    conditions = layers.Input(condition_shape, name="conditions")
    features = layers.concatenate([latent_vectors, conditions])

    features = layers.Dense(output_shape[0])(features)
    features = layers.Activation(layers.LeakyReLU(alpha=0.2))(features)

    features = layers.Reshape(output_shape)(features)

    for i in range(blocks):
        features = layers.Conv1D(
            filters=filters, kernel_size=3, dilation_rate=2 ** i, padding="same"
        )(features)
        features = layers.LeakyReLU(alpha=0.2)(features)
        features = layers.SpatialDropout1D(rate=0.2)(features)

    features = layers.Conv1D(filters=output_shape[1], kernel_size=3, padding="same")(
        features
    )
    samples = layers.Activation(final_activation)(features)

    generator = tf.keras.Model([latent_vectors, conditions], samples, name=name)
    generator.summary()
    return generator


def mnist_discriminator(
    sample_shape: Tuple[int] = (1024, 1),
    condition_shape: Tuple[int] = (10,),
    output_dim: int = 1,
    feature_width: int = 256,
    filters: int = 64,
    blocks: int = 6,
    final_activation=None,
    name: str = "discriminator",
):
    """
    Constructs and returns a conditional discriminator/critic to be used as part of a GAN

    Args:
        sample_shape: Tuple[int], Shape of a single data sample.
        condition_shape: Tuple[int], Shape of a single condition vector.
        output_dim: int, Expected number of output features.
        feature_width: int, Number of neurons in final dense layers.
        filters: int, Number of filters used in convolution layers.
        blocks: int, Number of WaveNet blocks to stack at the tail of the generator.
        final_activation: Activation function applied to network outputs.
        name: str, Model identifier.

    Returns: keras.models.Model
    """
    samples = layers.Input(sample_shape, name="samples")
    conditions = layers.Input(condition_shape, name="conditions")

    features = layers.concatenate(
        [samples, layers.RepeatVector(sample_shape[0])(conditions)]
    )

    for i in range(blocks):
        features = layers.Conv1D(
            filters=filters, kernel_size=4, strides=2, padding="same"
        )(features)
        features = layers.LeakyReLU(alpha=0.2)(features)
    features = layers.GlobalAveragePooling1D()(features)

    for _ in range(3):
        features = layers.Dropout(rate=0.25)(features)
        features = layers.Dense(feature_width)(features)
        features = layers.LeakyReLU(alpha=0.2)(features)
    validity = layers.Dense(output_dim, activation=final_activation)(features)

    d = tf.keras.Model([samples, conditions], validity, name=name)
    d.summary()
    return d
