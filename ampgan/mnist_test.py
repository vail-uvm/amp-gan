"""
Trains a CGAN to generate MNIST digits.
"""

import argparse

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf

import callbacks
import model


def get_parser():
    parser = argparse.ArgumentParser(
        description="Trains a GAN on reshaped MNIST images.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--batch_size",
        type=int,
        default=64,
        help="Number of samples per training batch.",
    )
    parser.add_argument(
        "--epochs",
        type=int,
        default=30,
        help="Number of times the training set is shown to the model.",
    )
    parser.add_argument(
        "--model_type",
        type=str.lower,
        default="gan",
        choices={"gan", "wgan"},
        help="GAN structure to use.",
    )

    return parser


def main(epochs: int = 30, batch_size: int = 64, model_type: str = "gan", **kwargs):
    (train_x, train_y), (test_x, test_y) = tf.keras.datasets.mnist.load_data()
    pad_dims = ((0, 0), (2, 2), (2, 2))
    train_x = np.pad(train_x, pad_dims).reshape((len(train_x), -1, 1)) / 255
    train_x = 2 * train_x - 1
    test_x = np.pad(test_x, pad_dims).reshape((len(test_x), -1, 1)) / 255
    test_x = 2 * test_x - 1
    train_y = tf.keras.utils.to_categorical(train_y, num_classes=10)
    test_y = tf.keras.utils.to_categorical(test_y, num_classes=10)

    dataset = tf.data.Dataset.from_tensor_slices(
        (train_x.astype("float32"), train_y.astype("float32"))
    )
    dataset = dataset.shuffle(buffer_size=1024).batch(batch_size).prefetch(32)

    if model_type == "gan":
        gan = model.CGAN(
            model.mnist_discriminator(final_activation="sigmoid"),
            model.mnist_generator(),
        )
    else:
        gan = model.CWGANGP(model.mnist_discriminator(), model.mnist_generator())
    gan.compile()
    gan.fit(
        dataset,
        epochs=epochs,
        callbacks=[
            callbacks.SampleVisualizer(output_dir=f"../results/mnist_gen_{model_type}")
        ],
    )
    gan.save(f"../models/mnist_{model_type}", save_format="tf")

    gan = tf.keras.models.load_model(f"../models/mnist_{model_type}")
    latent_vectors = tf.random.normal(shape=(100, 256))
    labels = tf.convert_to_tensor(np.eye(10)[np.repeat(np.arange(10), 10)])
    samples = gan.generator([latent_vectors, labels])
    samples = samples.numpy().reshape((len(samples), 32, 32))
    samples = (samples + 1) / 2
    fig, axes = plt.subplots(10, 10, sharex=True, sharey=True, figsize=(12, 12))
    for c in range(10):
        for i in range(10):
            axes[c, i].imshow(
                samples[c * 10 + i], cmap="binary",
            )
            axes[c, i].axis("off")
    fig.savefig(f"../results/mnist_gen_{model_type}/load_test.png")
    plt.close()


if __name__ == "__main__":
    args = vars(get_parser().parse_args())
    main(**args)
